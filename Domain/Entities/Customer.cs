﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
	public class Customer : Entity
	{
		public virtual Guid Id { get; private set; }
		public virtual string Name { get; protected set; }
		public virtual string Email { get; protected set; }

		public List<string> Validate()
		{
			Validations = new List<string>();
			
			if (this.Name == null || string.IsNullOrEmpty(this.Name.Trim())) this.AddEmptyValidationField("Name");

			if (this.Email == null || string.IsNullOrEmpty(this.Email.Trim())) this.AddEmptyValidationField("Email");
			
			return this.Validations;
		}
		public static Customer Create(string name, string email)
		{
			return Create(Guid.NewGuid(), name, email);
		}

		public static Customer Create(Guid id, string name, string email)
		{
			var customer = new Customer();
			customer.Id = id;
			customer.Name = name;
			customer.Email = email;

			var err = customer.Validate();
			customer.Validations = err;
			return customer;

		}
	}
}
