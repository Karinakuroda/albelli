﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Entities
{
	public class Product : Entity
	{
		public virtual Guid Id { get; protected set; }

		public virtual int Quantity { get; protected set; }
		public virtual decimal Cost { get; protected set; }
		public virtual string Code { get; protected set; }

		public Order Order { get; protected set; }
		public void UpdateOrder(Order order)
		{
			this.Order = order;
		}
		public List<string> Validate()
		{
			if (Validations == null) Validations = new List<string>();
			if (Quantity <= 0) AddEmptyValidationField("Quantity");
			if (string.IsNullOrEmpty(Code)) AddEmptyValidationField("Code");
			if (Cost <= 0) AddEmptyValidationField("Cost");

			return this.Validations;
		}

		private static Product Create(Guid id, int quantity, decimal cost, string code, Order order = null)
		{
			var product = new Product();
			

			product.Id = id;
			product.Quantity = quantity;
			product.Cost = cost;
			product.Code = code;
			product.Order = order;

			return product;
		}
		
		public static Product Create(int quantity, decimal cost, string code)
		{
			return Create(Guid.NewGuid(), quantity, cost, code);
		}

	}
}