﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Entities
{
	public class Order : Entity
	{
		public virtual Guid Id { get; protected set; }
		
		private decimal _price;

		public virtual decimal Price
		{
			get
			{
				return Products != null ? Products.Sum(p => p.Cost * p.Quantity) : 0;
			}
			set
			{
				_price = value;
			}
		}
		
		public virtual Customer Customer { get; protected set; }
		public virtual ICollection<Product> Products { get; protected set; }

		public List<string> Validate()
		{
			Validations = new List<string>();
			if (this.Customer == null || this.Customer.Id==Guid.Empty) this.AddEmptyValidationField("Customer");

			var productValidations = new List<string>();
			foreach (var product in Products)
				productValidations.AddRange(product.Validate());

			Validations.AddRange(productValidations);
			return Validations;
		}
		public static Order Create(Customer customer, List<Product> products)
		{
			var order = Order.Create(Guid.NewGuid(), customer, products);
			return order;
		}

		public static Order Create(Guid id, Customer customer, List<Product> products)
		{
			var order = new Order();
			order.Id = id;
			order.Customer = customer;
			order.Products = products;

			var err = order.Validate();
			order.Validations = err;
			return order;
		}

	}
}
