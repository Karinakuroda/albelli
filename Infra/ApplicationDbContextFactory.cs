﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace Infra
{
	public class ApplicationDbContextFactory : IDesignTimeDbContextFactory<AlbelliContext>
	{
		public AlbelliContext CreateDbContext(string[] args)
		{


			var configurationRoot = new ConfigurationBuilder()
											.AddJsonFile(Path.GetFullPath(Path.Combine(@"../Albelli/appsettings.json")), optional: false)
											.AddJsonFile(Path.GetFullPath(Path.Combine(@"../Albelli/appsettings.Development.json")), optional: true)
											.Build();

		 
			var builder = new DbContextOptionsBuilder<AlbelliContext>();
			builder.UseSqlServer(configurationRoot.GetConnectionString("DatabaseConnection"),
				optionsBuilder => optionsBuilder.MigrationsAssembly(typeof(AlbelliContext).GetTypeInfo().Assembly.GetName().Name));

			return new AlbelliContext(builder.Options);
		}
	}
}
