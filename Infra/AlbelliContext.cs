﻿using Domain.Entities;
using Domain.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Infra
{
    public class AlbelliContext: DbContext
	{
		public AlbelliContext(DbContextOptions<AlbelliContext> options)
		  : base(options)
		{ }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Order> Orders { get; set; }
		public DbSet<Product> Products{ get; set; }

	 
		

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Order>(o =>
			{
				o.Property(b => b.Price).HasColumnType("decimal(10, 2)");
				o.Property(b =>b.Created).HasDefaultValueSql("getutcdate()");
			});

			modelBuilder.Entity<Product>(p =>
			{
				p.Property(b => b.Cost).HasColumnType("decimal(10, 2)");
				p.Property(b => b.Created).HasDefaultValueSql("getutcdate()");
			});

			modelBuilder.Entity<Customer>(c =>
			{ 
				c.Property(b => b.Created).HasDefaultValueSql("getutcdate()");
			});

		}
	}

}
