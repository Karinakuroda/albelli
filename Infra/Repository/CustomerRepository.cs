﻿using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Interfaces.Repository;
using Microsoft.EntityFrameworkCore;

namespace Infra.Repository
{
	public class CustomerRepository : ICustomerRepository
	{
		private AlbelliContext _context { get; }
		public CustomerRepository(AlbelliContext context)
		{ 
			_context = context;
		}
		 
	 
		public bool Add(Customer customer)
		{
			_context.Add(customer);
			var result = _context.SaveChanges();
			return result > 0;
		}

		public Customer Get(Guid customerId)
		{
			return  _context.Customers.FirstOrDefault(s => s.Id == customerId);
		}

		public bool Remove(Guid customerId)
		{
			var cust =  Get(customerId);
			 _context.Remove(cust);
			return (_context.SaveChanges()>0);
		}

		public bool Update(Customer customer)
		{
			_context.Entry(customer).State = EntityState.Modified;
			_context.Entry(customer).Property(x => x.Created).IsModified = false;
			return (_context.SaveChanges() > 0);

		}

		public List<Customer> GetAll()
		{
			return _context.Customers.ToList();
		}
	}
}
