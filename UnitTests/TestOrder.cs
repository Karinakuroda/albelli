using System;
using System.Collections.Generic;
using System.Linq;
using Domain.Entities;
using Domain.Interfaces.Repository;
using Domain.Interfaces.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Service;
using FluentAssertions;

namespace UnitTests
{
	[TestClass]
    public class TestOrder
    {
		Mock<IOrderRepository> _mockOrderRepository;
		IOrderService _orderService;

		[TestInitialize]
		public void Init()
		{
			_mockOrderRepository = new Mock<IOrderRepository>();
			_orderService = new OrderService(_mockOrderRepository.Object);
		}

		#region Calls
		
		[TestMethod]
		public void ShouldCallGetOrderOnlyOnce()
        {
			//Arrange
			_mockOrderRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetOrderMock());
			//Act
			var resp = _orderService.Get(Guid.NewGuid());
			//Assert
			_mockOrderRepository.Verify(o => o.Get(It.IsAny<Guid>()), Times.Once());
		}

		[TestMethod]
		public void ShouldCallUpdateOrderOnlyOnce()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Update(It.IsAny<Order>())).Returns(true);
			//Act
			var resp = _orderService.Update(GetOrderMock());
			//Assert
			_mockOrderRepository.Verify(o => o.Update(It.IsAny<Order>()), Times.Once());
		}

		[TestMethod]
		public void ShouldCallRemoveOrderOnlyOnce()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Remove(It.IsAny<Guid>())).Returns(true);
			//Act
			var resp = _orderService.Remove(Guid.NewGuid());
			//Assert
			_mockOrderRepository.Verify(o => o.Remove(It.IsAny<Guid>()), Times.Once());
		}

		[TestMethod]
		public void ShouldNeverCallGetOrderWhenNullOrEmpty()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetOrderMock());
			//Act
			var resp = _orderService.Get(Guid.Empty);
			//Assert
			_mockOrderRepository.Verify(o => o.Get(It.IsAny<Guid>()), Times.Never());
		}

		[TestMethod]
		public void ShouldNeverCallUpdateOrderWhenNullOrEmpty()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Update(It.IsAny<Order>())).Returns(true);
			//Act
			var resp = _orderService.Update(GetOrderEmptyId());
			//Assert
			_mockOrderRepository.Verify(o => o.Update(It.IsAny<Order>()), Times.Never());
		}
		[TestMethod]
		public void ShouldNeverCallRemoveOrder()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Remove(It.IsAny<Guid>())).Returns(true);
			//Act
			var resp = _orderService.Remove(Guid.Empty);
			//Assert
			_mockOrderRepository.Verify(o => o.Remove(It.IsAny<Guid>()), Times.Never());
		}
		
		#endregion

		#region WhenGet

		[TestMethod]
		public void WhenGet_ShouldGetOrder()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetOrderMock());
			//Act
			var resp = _orderService.Get(Guid.NewGuid());
			//Assert
			Assert.AreEqual(GetOrderMock().Price, resp.Price);
			Assert.IsNotNull(resp);
		}

		[TestMethod]
		public void WhenGet_ShouldSumOrderPrice()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetOrderMock());
			//Act
			var resp = _orderService.Get(Guid.NewGuid());
			//Assert
			Assert.AreEqual(GetOrderMock().Products.Sum(p => p.Quantity * p.Cost), resp.Price);
		}

		[TestMethod]
		public void WhenGet_ShouldValidateNullIdCustomer()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Get(It.IsAny<Guid>()));
			//Act
			var resp = _orderService.Get(Guid.Empty);
			//Assert
			_orderService.Messages.Should().Contain(GetEmptyFieldValidation("Id"));
		}
		#endregion

		#region WhenPost


		[TestMethod]
		public void WhenPost_ShouldValidateProductQuantityEmpty()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Add(It.IsAny<Order>()));
			//Act
			var resp = _orderService.Add(GetOrderProductQuantityEmpty());
			//Assert
			_orderService.Messages.Should().Contain(GetEmptyFieldValidation("Quantity"));
		}
		[TestMethod]
		public void WhenPost_ShouldValidateProductCodeEmpty()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Add(It.IsAny<Order>()));
			//Act
			var resp = _orderService.Add(GetOrderProductCodeEmpty());
			//Assert
			_orderService.Messages.Should().Contain(GetEmptyFieldValidation("Code"));
		}
		[TestMethod]
		public void WhenPost_ShouldValidateProductCostEmpty()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Add(It.IsAny<Order>()));
			//Act
			var resp = _orderService.Add(GetOrderProductCostEmpty());
			//Assert
			_orderService.Messages.Should().Contain(GetEmptyFieldValidation("Cost"));
		}

		#endregion

		#region WhenPut

		[TestMethod]
		public void WhenPut_ShouldValidateNullIdCustomer()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Update(It.IsAny<Order>()));
			//Act
			var resp = _orderService.Update(GetOrderEmptyId());
			//Assert
			_orderService.Messages.Should().Contain(GetEmptyFieldValidation("Id"));
		}

		[TestMethod]
		public void WhenPut_ShouldValidateProductQuantityEmpty()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Update(It.IsAny<Order>()));
			//Act
			var resp = _orderService.Update(GetOrderProductQuantityEmpty());
			//Assert
			_orderService.Messages.Should().Contain(GetEmptyFieldValidation("Quantity"));
		}
		[TestMethod]
		public void WhenPut_ShouldValidateProductCodeEmpty()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Update(It.IsAny<Order>()));
			//Act
			var resp = _orderService.Update(GetOrderProductCodeEmpty());
			//Assert
			_orderService.Messages.Should().Contain(GetEmptyFieldValidation("Code"));
		}
		[TestMethod]
		public void WhenPut_ShouldValidateProductCostEmpty()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.Update(It.IsAny<Order>()));
			//Act
			var resp = _orderService.Update(GetOrderProductCostEmpty());
			//Assert
			_orderService.Messages.Should().Contain(GetEmptyFieldValidation("Cost"));
		}

		#endregion

		#region Private Methods
		private static Order GetOrderProductCodeEmpty()
		{
			var list = new List<Product>();
			list.Add(Product.Create(20, 10, string.Empty));
			return Order.Create(Customer.Create("Test", "Test@t.com"), list);
		}
		private static Order GetOrderProductQuantityEmpty()
		{
			var list = new List<Product>();
			list.Add(Product.Create(0, 10, "1234"));
			return Order.Create(Customer.Create("Test", "Test@t.com"), list);
		}
		private static Order GetOrderProductCostEmpty()
		{
			var list = new List<Product>();
			list.Add(Product.Create(1, 0, "1234"));
			return Order.Create(Customer.Create("Test", "Test@t.com"), list);
		}
		private static Order GetOrderMock()
		{

			var list = new List<Product>();
			list.Add(Product.Create(1, 10, "1234"));
			return Order.Create(Customer.Create("Test", "Test@t.com"), list);
		}
		private static Order GetOrderEmptyId()
		{
			var list = new List<Product>();
			list.Add(Product.Create(1, 10, "1234"));
			return Order.Create(Guid.Empty, Customer.Create("Test", "Test@t.com"), list);
		}
		private static string GetEmptyFieldValidation(string field)
		{
			return string.Format("{0} can't be empty.", field);
		}


		#endregion

	}
}
