using System;
using System.Collections.Generic;
using Domain.Entities;
using Domain.Interfaces.Repository;
using Domain.Interfaces.Service;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Service;
using FluentAssertions;

namespace UnitTests
{
	[TestClass]
    public class TestCustomer
    {
		Mock<ICustomerRepository> _mockCustomerRepository;
		Mock<IOrderRepository> _mockOrderRepository;
		ICustomerService _customerService;

		[TestInitialize]
		public void Init()
		{
			_mockCustomerRepository = new Mock<ICustomerRepository>();
			_mockOrderRepository = new Mock<IOrderRepository>();
			_customerService = new CustomerService(_mockCustomerRepository.Object, _mockOrderRepository.Object);
		}

		#region Calls
		
		[TestMethod]
		public void ShouldCallGetCustomerOnlyOnce()
        {
			//Arrange
			_mockCustomerRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetCustomerMock());
			//Act
			var resp = _customerService.Get(Guid.NewGuid());
			//Assert
			_mockCustomerRepository.Verify(o => o.Get(It.IsAny<Guid>()), Times.Once());
		}
		[TestMethod]
		public void ShouldCallGetAllCustomersRepositoryOnlyOnce()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.GetAll()).Returns(GetCustomerMockList());
			//Act
			var resp = _customerService.GetAll();
			//Assert
			_mockCustomerRepository.Verify(o => o.GetAll(), Times.Once());
		}
		[TestMethod]
		public void ShouldCallGetByCustomerOnlyOnce()
		{
			//Arrange
			_mockOrderRepository.Setup(s => s.GetByCustomer(It.IsAny<Guid>())).Returns(GetOrdersMock());
			_mockCustomerRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetCustomerMock());
			//Act
			var resp = _customerService.GetWithOrders(Guid.NewGuid());
			//Assert
			_mockOrderRepository.Verify(o => o.GetByCustomer(It.IsAny<Guid>()), Times.Once());
		}
		[TestMethod]
		public void ShouldCallGetRepositoryOnlyOnce()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetCustomerMock());
			//Act
			var resp = _customerService.GetWithOrders(Guid.NewGuid());
			//Assert
			_mockCustomerRepository.Verify(o => o.Get(It.IsAny<Guid>()), Times.Once());
		}
		[TestMethod]
		public void ShouldCallUpdateCustomerOnlyOnce()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Update(It.IsAny<Customer>())).Returns(true);
			//Act
			var resp = _customerService.Update(GetCustomerMock());
			//Assert
			_mockCustomerRepository.Verify(o => o.Update(It.IsAny<Customer>()), Times.Once());
		}

		[TestMethod]
		public void ShouldNeverCallGetCustomerWhenNullOrEmpty()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetCustomerMock());
			//Act
			var resp = _customerService.Get(Guid.Empty);
			//Assert
			_mockCustomerRepository.Verify(o => o.Get(It.IsAny<Guid>()), Times.Never);
		}
		[TestMethod]
		public void ShouldNeverCallUpdateCustomerWhenNullOrEmpty()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Update(It.IsAny<Customer>()));
			//Act
			var resp = _customerService.Update(GetCustomerEmptyId());
			//Assert
			_mockCustomerRepository.Verify(o => o.Update(It.IsAny<Customer>()), Times.Never);
		}
		[TestMethod]
		public void ShouldNeverCallRemoveCustomerWhenNullOrEmpty()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Remove(It.IsAny<Guid>()));
			//Act
			var resp = _customerService.Remove(Guid.Empty);
			//Assert
			_mockCustomerRepository.Verify(o => o.Remove(It.IsAny<Guid>()), Times.Never);
		}

		#endregion

		#region WhenPost
		[TestMethod]
		public void WhenPost_ShouldNotAllowEmptyName()
		{
			//Act
			var resp = _customerService.Add(GetCustomerEmptyName());
			//Assert
			resp.Should().BeFalse();
			_mockCustomerRepository.Verify(s => s.Add(It.IsAny<Customer>()), Times.Never);
		}
		[TestMethod]
		public void WhenPost_ShouldNotAllowEmptyEmail()
		{
			//Arrange 
			var customer = GetCustomerEmptyEmail();
			//Act
			var resp = _customerService.Add(customer);
			//Assert
			_mockCustomerRepository.Verify(s => s.Add(It.IsAny<Customer>()), Times.Never);
			resp.Should().BeFalse();
		}
		#endregion

		#region Update
		[TestMethod]
		public void WhenPut_ShouldNotAllowEmptyName()
		{
			//Arrange 
			_mockCustomerRepository.Setup(s => s.Update(GetCustomerEmptyName())).Returns(true);
			//Act
			var resp = _customerService.Update(GetCustomerEmptyName());
			//Assert
			resp.Should().BeFalse();
		}

		[TestMethod]
		public void WhenPut_ShouldNotAllowEmptyEmail()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Update(GetCustomerEmptyEmail())).Returns(true);
			//Act
			var resp = _customerService.Update(GetCustomerEmptyEmail());
			//Assert
			resp.Should().BeFalse();
		}
		[TestMethod]
		public void WhenPut_ShouldValidateNullIdCustomer()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Update(It.IsAny<Customer>()));
			//Act
			var resp = _customerService.Update(GetCustomerEmptyId());
			//Assert
			_customerService.Messages.Should().Contain(GetEmptyFieldValidation("Id"));
		}
		#endregion

		#region WhenGet

		[TestMethod]
		public void WhenGet_ShouldGetAllCustomersRepository()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.GetAll()).Returns(GetCustomerMockList());
			//Act
			var resp = _customerService.GetAll();
			//Assert
			Assert.AreEqual(2, resp.Count);
		}
		[TestMethod]
		public void WhenGet_ShouldGetCustomer()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetCustomerMock());
			//Act
			var resp = _customerService.Get(Guid.NewGuid());
			//Assert
			Assert.AreEqual(GetCustomerMock().Email,resp.Email);
		}
		[TestMethod]
		public void WhenGet_ShouldValidateNullIdCustomer()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Get(It.IsAny<Guid>())).Returns(GetCustomerMock());
			//Act
			var resp = _customerService.Get(Guid.Empty);
			//Assert
			_customerService.Messages.Should().Contain(GetEmptyFieldValidation("Id"));
		}

		#endregion

		[TestMethod]
		public void ShouldAddMessageWhenEmptyName()
		{
			//Arrange 
			_mockCustomerRepository.Setup(s => s.Add(GetCustomerEmptyName())).Returns(true);
			//Act
			var resp = _customerService.Add(GetCustomerEmptyName());
			//Assert
			_customerService.Messages.Should().Contain(GetEmptyFieldValidation("Name"));
		}
		[TestMethod]
		public void ShouldAddMessageWhenEmptyEmail()
		{
			//Arrange 
			_mockCustomerRepository.Setup(s => s.Add(GetCustomerEmptyEmail())).Returns(true);
			//Act
			var resp = _customerService.Add(GetCustomerEmptyEmail());
			//Assert
			_customerService.Messages.Should().Contain(GetEmptyFieldValidation("Email"));
		}



		[TestMethod]
		public void ShouldValidateNullEntity()
		{
			//Arrange
			_mockCustomerRepository.Setup(s => s.Add(Customer.Create("", ""))).Returns(true);
			//Act
			var resp = _customerService.Add(Customer.Create("", ""));
			//Assert
			Assert.AreEqual(false, resp);
		}

		#region Private Methods
		private static Customer GetCustomerEmptyId()
		{
			return Customer.Create(Guid.Empty, "", "");
		}
		private static Customer GetCustomerEmptyName()
		{
			return Customer.Create("", "carlosnorris@gmail.com");
		}
		private static Customer GetCustomerEmptyEmail() {
			return Customer.Create("Carlos Norris", "");
		}
		private static Customer GetCustomerMock()
		{
			return Customer.Create("Test", "Test@t.com");
		}
		private static List<Customer> GetCustomerMockList()
		{
			var list = new List<Customer>();
			list.Add(Customer.Create("Test", "Test@t.com"));
			list.Add(Customer.Create("Test2", "Test2@t.com"));
			return list;
		}
		private static List<Order> GetOrdersMock()
		{
			var list = new List<Order>();
			list.Add(Order.Create(GetCustomerMock(), GetProductsMock()));
			list.Add(Order.Create(GetCustomerMock(), GetProductsMock()));
			return list;
		}
		private static List<Product> GetProductsMock()
		{
			var list = new List<Product>();
			list.Add(Product.Create(1,10,"2323"));
			list.Add(Product.Create(1, 5, "2323"));
			return list;
		}
		private static string GetEmptyFieldValidation(string field) {
			return string.Format("{0} can't be empty.", field);
		}
		#endregion
	}
}
