# Albelli's Test


## Getting Started

You can use this app via Swagger

### Dependencies

```
.NET CORE 2.0
Web API
AutoMapper
Entity Framework Core
Sql Server Express
Swagger
DDD


Fluent
```

### Running


```
dotnet run API.dll
```
