﻿using System;
using System.Net;
using AutoMapper;
using Domain.Entities;
using Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;

namespace Albelli.Controllers
{
	[Route("api/[controller]")]
    public class OrdersController : Controller
    {
		private IOrderService _orderService;
		private IMapper _mapper; 

		public OrdersController(IOrderService orderService, IMapper mapper) {
			_orderService = orderService;
			_mapper = mapper; 
		}

        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
			var order =  _orderService.Get(id);
			var model = _mapper.Map<Order, API.Models.OrderResponse>(order);
			return Ok(model);
		}
		
		[HttpPost]
		public IActionResult Post([FromBody] API.Models.OrderRequest order)
		{
			try
			{
				bool success = false;
				var model = _mapper.Map<API.Models.OrderRequest, Order>(order);
				success = _orderService.Add(model);

				if (success)
					return Ok(_orderService.Messages);
				else
				{
					if (_orderService.Messages.Count > 0)
					{
						return StatusCode((int)HttpStatusCode.BadRequest, _orderService.Messages);
					}
					return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException("Can't Save Order"));
				}
			}
			catch (Exception ex)
			{
				return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException(ex.ToString()));
			}
		}

		[HttpPut]
		public IActionResult Put([FromBody]API.Models.OrderRequest order)
		{

			try
			{
				bool success = false;
				var model = _mapper.Map<API.Models.OrderRequest, Order>(order);
				success = _orderService.Update(model);

				if (success)
					return Ok(_orderService.Messages);
				else
				{
					if (_orderService.Messages.Count > 0)
					{
						return StatusCode((int)HttpStatusCode.BadRequest, _orderService.Messages);
					}
					return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException("Can't Update Order"));
				}
			}
			catch (Exception ex)
			{
				return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException(ex.ToString()));
			}

		}
		
		[HttpDelete("{id}")]
		public IActionResult Delete(Guid id)
		{
			try
			{
				bool success = false;
				success = _orderService.Remove(id);

				if (success)
					return Ok(_orderService.Messages);
				else
					return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException("Can't Delete Order"));
			}
			catch (Exception ex)
			{
				return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException(ex.ToString()));
			}
		}
	}
}
