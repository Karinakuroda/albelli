﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using API.Mappings;
using AutoMapper;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Interfaces.Service;
using Microsoft.AspNetCore.Mvc;

namespace Albelli.Controllers
{
    [Route("api/[controller]")]
    public class CustomersController : Controller
    {
		private ICustomerService _customerService;
		private IMapper _mapper;

		public CustomersController(ICustomerService customerService,
								  IMapper mapper) {
			_customerService = customerService;
			_mapper = mapper; 
		}
		
        [HttpGet("{id}")]
        public IActionResult Get(Guid id)
        {
			var customer =  _customerService.Get(id);
			if (customer != null)
			{
				var model = _mapper.Map<Customer, API.Models.CustomerResponse>(customer);
				return Ok(model);
			}
			return  StatusCode((int)HttpStatusCode.NotFound);

		}
			
		[HttpGet]
		[Route("Get")]
		public IActionResult Get()
		{
			var customersList = _customerService.GetAll();
			if (customersList != null)
			{
				var model = _mapper.Map<List<Customer>, List<API.Models.CustomerResponse>>(customersList);
				return Ok(model);
			}
			return StatusCode((int)HttpStatusCode.NotFound);
		}

		[HttpGet]
		[Route("{id}/Orders")]
		public IActionResult GetWithOrders(Guid id)
		{
			var customer = _customerService.GetWithOrders(id);
			if (customer != null)
				return Ok(customer);
			
			return StatusCode((int)HttpStatusCode.NotFound);
		}

		[HttpPost]
		public IActionResult Post([FromBody] API.Models.CustomerRequest customer)
		{
			try
			{
				bool success = false;
				var model = _mapper.Map<API.Models.CustomerRequest, Customer>(customer);
				success = _customerService.Add(model);

				if (success)
					return Ok(_customerService.Messages);
				else
				{
					if (_customerService.Messages.Count>0)
					{
						return StatusCode((int)HttpStatusCode.BadRequest, _customerService.Messages);
					}
					return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException("Can't Save Customer"));
				}
			}
			catch (Exception ex)
			{
				return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException(ex.ToString()));
			}
		}
		 
		[HttpPut]
        public IActionResult Put([FromBody]API.Models.CustomerRequest customer)
        {
			try
			{
				bool success = false;
				var model = _mapper.Map<API.Models.CustomerRequest, Customer>(customer);
				success = _customerService.Update(model);

				if (success)
					return Ok(_customerService.Messages);
				else
				{
					if (_customerService.Messages.Count > 0)
					{
						return StatusCode((int)HttpStatusCode.BadRequest, _customerService.Messages);
					}
					return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException("Can't Update Customer"));
				}
			}
			catch (Exception ex)
			{
				return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException(ex.ToString()));
			}

		}
		
        [HttpDelete("{id}")]
        public IActionResult Delete(Guid id)
        {
			try
			{
				bool success = false;
				success = _customerService.Remove(id);

				if (success)
					return Ok(_customerService.Messages);
				else
					return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException("Can't Delete Customer"));
			}
			catch (Exception ex)
			{
				return StatusCode((int)HttpStatusCode.InternalServerError, new System.ApplicationException(ex.ToString()));
			}
		}
    }
}
