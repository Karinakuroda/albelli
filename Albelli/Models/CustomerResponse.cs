﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class CustomerResponse
    {
		public Guid Id { get;  set; }
		public string Name { get;  set; }
		public string Email { get;  set; }

		public DateTime Created { get; set; }
		public DateTime Updated { get; set; }

	}
}
