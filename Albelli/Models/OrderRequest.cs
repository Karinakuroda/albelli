﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class OrderRequest
    {
		public Guid Id { get;  set; }
		public OrderCustomerRequest Customer { get;  set; }
		public ICollection<ProductRequest> Products { get;  set; }
	}
	public class OrderCustomerRequest
	{
		public Guid Id { get; set; }
	}
}
