﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Models
{
    public class ProductRequest
    { 
		public decimal Quantity { get;  set; }
		public decimal Cost { get; set; }
		public string Code{ get; set; }

	}
}
