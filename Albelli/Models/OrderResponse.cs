﻿using System;
using System.Collections.Generic;

namespace API.Models
{
	public class OrderResponse
    {
		public Guid Id { get;  set; }
		public decimal Price { get; set; }
		public DateTime Created { get; set; }
		public DateTime Updated { get; set; }

		public ICollection<ProductResponse> Products { get;  set; }

	}
}
