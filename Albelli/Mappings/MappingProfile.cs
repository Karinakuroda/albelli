﻿using AutoMapper;
using Domain.Entities;

namespace API.Mappings
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<Product, API.Models.ProductResponse>();
			CreateMap<API.Models.ProductRequest, Product>()
				.ForMember(dest => dest.Validations, opt => opt.Ignore())
				.ForMember(dest => dest.Created, opt => opt.Ignore())
				.ForMember(dest => dest.Updated, opt => opt.Ignore())
				.ForMember(dest => dest.Order, opt => opt.Ignore());

			CreateMap<Customer, API.Models.CustomerResponse>();
			CreateMap<API.Models.CustomerRequest, Customer>()
				.ForMember(dest => dest.Validations, opt => opt.Ignore())
				.ForMember(dest => dest.Created, opt => opt.Ignore())
				.ForMember(dest => dest.Updated, opt => opt.Ignore())
				.ForMember(dest => dest.Name, opt => opt.MapFrom(m => string.Format("{0} {1}", m.FirstName, m.LastName)));


			CreateMap<API.Models.OrderCustomerRequest, Customer>();
			CreateMap<Order, API.Models.OrderResponse>();
			CreateMap<API.Models.OrderRequest, Order>() 
				.ForMember(dest => dest.Validations, opt => opt.Ignore())
				.ForMember(dest => dest.Created, opt => opt.Ignore())
				.ForMember(dest => dest.Updated, opt => opt.Ignore())
				.ForMember(dest => dest.Customer, opt => opt.MapFrom(m => m.Customer))
				.ForMember(dest => dest.Products, opt => opt.MapFrom(m => m.Products));
			
		}
	}
}
