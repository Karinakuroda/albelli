﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Service
{
	public abstract class BaseService
	{
		public List<string> Messages { get; private set; }

		public BaseService()
		{
			this.Messages = new List<string>();
		}

		public void AddEmptyValidationField(string field)
		{
			this.Messages.Add(string.Format("{0} can't be empty.",field));
		}
	
	}
}
