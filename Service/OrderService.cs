﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Domain.Entities;
using Domain.Interfaces;
using Domain.Interfaces.Repository;
using Domain.Interfaces.Service;

namespace Service
{
	public class OrderService : BaseService, IOrderService
	{
		private IOrderRepository _orderRepository;
		
		public OrderService(IOrderRepository orderRepository)
			:base()
		{
			_orderRepository = orderRepository;
		}

		public bool Add(Order order)
		{
			var err = order.Validate();
			if (err.Count == 0)
				return _orderRepository.Add(order);
			else
				this.Messages.AddRange(err);

			return false;
		}

		public Order Get(Guid orderId)
		{
			if (orderId == Guid.Empty)
			{
				AddEmptyValidationField("Id");
				return null;
			}
			return _orderRepository.Get(orderId);
		}

		public bool Remove(Guid orderId)
		{
			if (orderId == Guid.Empty)
			{
				AddEmptyValidationField("Id");
				return false;
			}
			return _orderRepository.Remove(orderId);
		}

		public bool Update(Order order)
		{
			if (order.Id != Guid.Empty)
			{
				var err = order.Validate();
				if (err.Count == 0)
				{
					foreach (var product in order.Products)
						product.UpdateOrder(order);

					return _orderRepository.Update(order);
				}
				else
					this.Messages.AddRange(err);
			}
			else
				AddEmptyValidationField("Id");

			return false;
		}
	}
}
