﻿using System;
using System.Collections.Generic;
using Domain.DTO;
using Domain.Entities;
using Domain.Interfaces.Repository;
using Domain.Interfaces.Service;

namespace Service
{
	public class CustomerService : BaseService, ICustomerService
	{
		private ICustomerRepository _customerRepository;
		private IOrderRepository _orderRepository;
		

		public CustomerService(ICustomerRepository customerRepository, IOrderRepository orderRepository)
			:base()
		{
			_customerRepository = customerRepository;
			_orderRepository = orderRepository;
		}


		public bool Add(Customer customer)
		{
			var err = customer.Validate();
			if (err.Count == 0)
				return _customerRepository.Add(customer);
			else
				this.Messages.AddRange(err);

			return false;

		}

		public Customer Get(Guid customerId)
		{
			if (customerId == Guid.Empty)
			{
				AddEmptyValidationField("Id");
				return null;
			}
			return _customerRepository.Get(customerId);
		}

		public bool Remove(Guid customerId)
		{
			if (customerId == Guid.Empty)
			{
				AddEmptyValidationField("Id");
				return false;
			}
			return _customerRepository.Remove(customerId);
		}

		public bool Update(Customer customer)
		{
			if (customer.Id != Guid.Empty)
			{
				var err = customer.Validate();
				if (err.Count == 0)
					return _customerRepository.Update(customer);
				else
					this.Messages.AddRange(err);
			}
			else
				AddEmptyValidationField("Id");

			return false;

		}

		public List<Customer> GetAll()
		{
			return _customerRepository.GetAll();
		}

		public CustomerOrders GetWithOrders(Guid customerId)
		{
			var response = new CustomerOrders();
			var customer = Get(customerId);
			if (customer != null)
			{
				response.Id = customerId;
				response.Name = customer.Name;
				response.Email = customer.Email;

				var orders = _orderRepository.GetByCustomer(customerId);
				response.Orders = orders;
			}
			return response;
		}
	}
}
